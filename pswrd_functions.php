<!-- PHP -->
<?php
require_once($phpbb_root_path . 'includes/functions_privmsgs.' . $phpEx);

// ==================================== VARIABLES
//farbe, die die zeile hat, wenn sie markiert wird
$mark_color = '#d32f2f';
$tablehead_fontsize = '1.1em';
$tablebody_fontsize = '1em';
$maxid=0;
$scell = '<td>';
$ecell = '</td>';



// ==================================== FUINCTIONS
function setcock($cookiename, $cookievalue){
  $_SESSION[$cookiename] = $cookievalue;
  // setcookie($cookiename, $cookievalue, 0, '/');
}

function make_bbcode_uid()
{
   // Unique ID for this message..

   $uid = md5(mt_rand());
   $uid = substr($uid, 0, 8);

   return $uid;
}

function send_private_message($sender_id, $sender_ip = '127.0.0.1', $sender_username, $recipient_id, $subject, $message, $enable_signature = FALSE, $enable_bbcode = TRUE, $enable_smilies = TRUE, $enable_urls = TRUE){

  #DO CONFIG VARIABLE CHECKS
  $uid = $bitfield = $options = '';
  #this function is from functions_content which is already loaded by common.php
  generate_text_for_storage($message, $uid, $bitfield, $options, $enable_bbcode, $enable_urls, $enable_smilies);

  $data = array(
     'from_user_id'      => $sender_id,
     'from_user_ip'      => $sender_ip,
     'from_username'      => $sender_username,
     'enable_sig'      => $enable_signature,
     'enable_bbcode'      => $enable_bbcode,
     'enable_smilies'   => $enable_smilies,
     'enable_urls'      => $enable_urls,
     'icon_id'         => 0,
     'bbcode_bitfield'   => $bitfield,
     'bbcode_uid'      => $uid,
     'message'         => $message,
     'address_list'      => array('u' => array($recipient_id => 'to'))
  );

   submit_pm('post', $subject, $data, FALSE);
     // print_r($subject);
		 // echo "<BR><BR>";
		 // print_r($data);

}


function error_pm($ermes){
  send_private_message($sender_id, $sender_ip, $sender_username, $recipient_id, $subject, $ermes, $enable_signature = FALSE, $enable_bbcode = TRUE, $enable_smilies = TRUE, $enable_urls = TRUE);
}

function mes($content){
  echo '<h3>'. $content . '</h3>';
}

function mes_center($contenti){
  echo '<h3 style="margin:auto; text-align: center;">'. $contenti . '</h3>';
}
?>
<!-- ENDPHP -->
