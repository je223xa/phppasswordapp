<!-- PHP -->
<?php
// i need a variable, that tells me which table im currently in. otherwise the script doesnt know where to save stuff
// so now theres a cookie variable called $_SESSION['tablename']
// thats the name of the active table
error_reporting(E_ALL);
error_reporting(-1);
// ccreated by: Jonathan Ebinger
$phpbb_root_path = "../betech/";
$phpEx = 'php';
include($phpbb_root_path . 'config.' . $phpEx);
require_once($phpbb_root_path . 'common.' . $phpEx);
require_once($phpbb_root_path . 'phpbb/session.' . $phpEx);
require_once($phpbb_root_path . 'includes/functions_mcp.' . $phpEx);
include $phpbb_root_path . 'pswrd/pswrd_functions.' . $phpEx;
include $phpbb_root_path . 'pswrd/pswrd_stupidechos.' . $phpEx;
// the variables are taken from the user.php, then i dont have to hardcode any passwords
$db = new mysqli($dbhost, $dbuser, $dbpasswd, $dbname);
if ($db->connect_error) {
  die("Connection failed: " . $db->connect_error);
}

// =====================fehleranalyse
echo 'Du bist Gruppe: ' . $_SESSION['group'] . '<br>';
echo 'Du bist User: ' . $_SESSION['user-id'] . '<br>';
echo 'Du heisst: ' . $_SESSION['user-name']. '<br>';
echo '<br>';

// Email details, all variables for the function
if (isset($_SESSION['user-id'])) { // set this before access, otherwise error
  $sender_id = $_SESSION['user-id'];
  $sender_ip ='localhost';
  $sender_username =  $_SESSION['user-name'];
  $recipient_id = '2643';
  // if current user is no admin
  $admin_yes = ($_SESSION['group']==5) ? true : false;
}

// styles for html
styleset();
// div for input
div_inputform();
div_outputform();

function setcocks(){
  setcock('cKategorie', $_POST["Kategorie"]);
  setcock("cSemester", $_POST["Semester"]);
  setcock("tablename", "psw" . $_POST["Kategorie"] . $_POST["Semester"]);
}

$_SESSION['tablename'] = 'pswGRDSem1';
echo 'ich: ' . $_POST["Kategorie"];
// =============================================SELECT=============================================
// wehen button pressed
if(isset($_POST['outputsth'])){
  // select table from database
  // take name of table from chosen options, these are in the phpBB database
  // in the vars are never the names from the 'value' tag! the actual names are in the array up there. so i have the long names for selecting and the short ones for the variables
  // now i can always just take them from the sql table, which i created myself
  // now i just make sure the user selects the right table
  if($_POST["Kategorie"] <> 'GRD' && ($_POST["Semester"]  == 'Sem1' | $_POST["Semester"]  == 'Sem2' | $_POST["Semester"]  == 'Sem3')){
    $_POST["Kategorie"] = 'GRD';
    mes('Die ersten drei Semester sind nicht getrennt, ich wechsle für dich zum Grundstudium.....');
    echo '<br>';
    $tablename = "psw" . $_POST["Kategorie"] . $_POST["Semester"];
    header("Refresh:1");
  }elseif($_POST["Kategorie"] == 'GRD' && ($_POST["Semester"]  == 'Sem4' | $_POST["Semester"]  == 'Sem5')){
    mes('Das geht leider nicht, für die höheren Semester musst du einen Schwerpunkt auswählen. Versuchs nochmal');
    echo '<br>';
    $_POST["Kategorie"] = 'GRD';
    $_POST["Semester"] = 'Sem1';
    setcocks();
    header("Refresh:2");
  }else{
    // $tablename = "psw" . $_POST["Kategorie"] . $_POST["Semester"];
    // // setcocks();
    // $_SESSION['cKategorie'] = $_POST["Kategorie"];
    // $_SESSION['cSemester'] = $_POST["Semester"];
    // $_SESSION['tablename'] = "psw" . $_POST["Kategorie"] . $_POST["Semester"];
    //   header("Refresh:0");
  }
}

// =============================================TABLENAME=============================================
// this always gets executes, when cookievar tablename is set
if(isset($_SESSION['tablename'])){
  $tablename = $_SESSION['tablename'];
  $sql = 'SELECT * FROM ' . $tablename;
  // now its querytime!
  // Yo! keyword 'query' means mysql. there are different functions for different DBs
  $result = $db->query($sql);
  // now the return is checked, and the table is created from the sql data
  if ($result->num_rows > 0) {
      // create table with attributes and header
      echo '<div id="pswrdtable">'; // this is the header
      if($admin_yes){
        // if the current user is admin, he gets a few more table columns to delete rows and stuff
      $tablhead = '<table><tr style="font-size:'. $tablehead_fontsize .'"><th class="thead">Fach</th><th class="thead">Professor</th><th class="thead">Passwort</th><th class="thead">Zeile melden</th><th class="thead">admin_löschen</th><th class="thead">erstellt von</th></tr>';
      }
      else{
        // no admin, no column!
        $tablhead = '<table><tr style="font-size:'. $tablehead_fontsize .'"><th class="thead">Fach</th><th class="thead">Professor</th><th class="thead">Passwort</th><th class="thead">Zeile melden</th></tr>';
      }
      echo $tablhead; // create tablehead, admin or not
      while($row = $result->fetch_assoc()) { // query for select of the while table
        $table_line = ($row["mark"]==1) ? '<tr style="font-size:'. $tablebody_fontsize .'; background-color: '. $mark_color . '  ">' : '<tr style="font-size:'. $tablebody_fontsize .';">'; // when a row is marked, backrung turns to yellow, that easier for everyone to see
        $markstr = ($row["mark"]==1) ? 'demarkieren' : 'markieren' ;
        echo $table_line; // yellow or not yellow, thats the question!
        echo $scell . $row["subject"] . $ecell;           // row for suject
        echo $scell . $row["professor"] . $ecell;         // row for professor name
        echo $scell . $row["password"] . $ecell;         // row for password
        // have a button to mark a row as wrong or outdated
        echo $scell;
        echo '<form class="melden_'. $row["id"] .'" method="POST">';
        echo '<input style="background-color: Transparent;" class ="but_meldsth" name="meldentry_'. $row["id"] . '" type="submit" value=" '. $markstr . ' "/>'; // background-color
        echo '</form>'; // end of forms
        echo $ecell;
        if($admin_yes){ // add last column,when user is admin, he can delete the row
          // and he also gets to see which user added the name
        echo $scell;
        echo '<form class="del_'. $row["id"] .'" method="POST">';
        echo '<input style="background-color: Transparent;" class ="but_deldsth" name="deldentry_'. $row["id"] . '" type="submit" value="delete [id:' . $row["id"] . ']"/>';
        echo '</form>';
        echo $ecell;
        echo $scell;
        echo $row["user-id"];
        echo $ecell;
        }
        echo '</tr>'; // end of a tablecolumn
        $maxid = ($row["id"]>$maxid) ? $row["id"] : $maxid ; // everyid thats bigger, it just increments
        }
      // end of table
      echo '</table>';
      echo '</div>';

    }
    else{
      // empty means empty
      mes_center('Hier sind leider noch keine Passwörter drin');
    }
}


else{
  // when nothing is chosen
  mes_center('Bitte wähle zuerst eine Tabelle aus');
}
// =============================================TABLENAME=============================================



?>
<!-- ENDPHP -->
