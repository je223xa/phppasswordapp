<?php
error_reporting(E_ALL);
error_reporting(-1);
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
// $request->enable_super_globals();
$user->session_begin();
$auth->acl($user->data);
$user->setup();
if ($user->data['user_id'] == ANONYMOUS)
{
    login_box('', $user->lang['LOGIN']);
}
$_SESSION["group"] = $user->data['group_id'];
$_SESSION["user-id"] = $user->data['user_id'];
$_SESSION['user-name'] = $user->data['username'];
page_header('Passwoerter');

$template->set_filenames(array(
    'body' => 'pswrd_body.html',
));


make_jumpbox(append_sid("{$phpbb_root_path}viewforum.$phpEx"));
page_footer();
?>
